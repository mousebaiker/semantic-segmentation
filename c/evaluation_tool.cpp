#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <map>
#include <tuple>

using std::tie;
using std::map;
using std::string;
using namespace cv;

const string VAIH = "/home/aleksei/semantic-segmentation/Vaihingen/contest/";

struct comp {
  bool operator()(const Vec3b a, const Vec3b b) const {
    return tie(a[0], a[1], a[2]) < tie(b[0], b[1], b[2]);
  }
};

int main(int argc, char** argv) {
  string test_path, ground_path;
  if (argc == 1) {
    test_path = VAIH + "gts_for_participants/" +
    "top_mosaic_09cm_area1_spoiled.tif";
    ground_path = VAIH + "gts_for_participants/" +
    "top_mosaic_09cm_area1.tif";

  }
  else if (argc == 3) {
    test_path = std::string(argv[1]);
    ground_path = std::string(argv[2]);
  }
  else {
    std::cerr << "Invalid number of arguments. Please pass no arguments"
        "or 2 arguments, containing paths to the images." << std::endl;
    return 1;
  }
  Mat test_image = imread(test_path);
  Mat ground_truth = imread(ground_path);

  if (!test_image.data || !ground_truth.data) {
    std::cerr << "Cannot open the images." << std::endl;
    return 2;
  }

  map<Vec3b, int, comp> classes;
  classes[Vec3b(255, 255, 255)] = 0;
  classes[Vec3b(255, 0, 0)] = 1;
  classes[Vec3b(255, 255, 0)] = 2;
  classes[Vec3b(0, 255, 0)] = 3;
  classes[Vec3b(0, 255, 255)] = 4;
  classes[Vec3b(0, 0, 255)] = 5;


  Mat confusion_matrix = Mat::zeros(classes.size(), classes.size(), CV_32SC1);
  int result = 0;
  for (auto test_it = test_image.begin<Vec3b>(),
       test_end = test_image.end<Vec3b>(),
       truth_it = ground_truth.begin<Vec3b>();
       test_it != test_end; ++test_it, ++truth_it) {
    int predicted_value = classes[(*test_it)];
    int true_value = classes[(*truth_it)];
    confusion_matrix.at<int>(predicted_value, true_value) += 1;
  }

  std::cout << confusion_matrix << std::endl;

}
