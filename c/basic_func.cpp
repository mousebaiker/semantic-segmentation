/****
* basic_func.cpp
*
* Rotates picture and applies gaussian and median blurs to it.
*
* Aleksei Kalinov, 2016
**/
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char* argv[])
{
    Mat input;
    if (argc == 1)
    {
	     input = imread("../test/man.jpg");
    }
    else
    {
      input = imread(argv[1]);
    }

    if (!input.data)
    {
       return 1;
    }
    imshow("Input picture", input);
    waitKey(0);

    GaussianBlur(input, input, Size(5,5), 10);
    medianBlur(input, input, 5);

    Mat resized = Mat::zeros(input.rows, input.cols, input.type());
    double angle = 15.0;
    double scale = 1;
    Point center = Point(input.cols/2, input.rows/2);

    // Resizing and rotating.
    Mat rotation = getRotationMatrix2D(center, angle, scale);
    warpAffine(input, resized, rotation, resized.size());
    imshow("Rotated and resized", resized);
    imwrite("rotated.jpg", resized);
    waitKey(0);
}
