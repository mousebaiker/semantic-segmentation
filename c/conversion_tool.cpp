#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <map>
#include <tuple>

using std::tie;
using std::map;
using std::string;
using namespace cv;


struct comp {
  bool operator()(const Vec3b a, const Vec3b b) const {
    return tie(a[0], a[1], a[2]) < tie(b[0], b[1], b[2]);
  }
};

int main(int argc, char** argv) {
  if (argc < 3) {
    std::cerr << "Pass input image and destination path as a parameter!" << std::endl;
    return 1;
  }

  Mat test_image = imread(argv[1]);

  if(!test_image.data) {
    std::cerr << "Can't open image!" << std::endl;
    return 2;
  }

  map<Vec3b, int, comp> classes;
  classes[Vec3b(255, 255, 255)] = 0;
  classes[Vec3b(255, 0, 0)] = 1;
  classes[Vec3b(255, 255, 0)] = 2;
  classes[Vec3b(0, 255, 0)] = 3;
  classes[Vec3b(0, 255, 255)] = 4;
  classes[Vec3b(0, 0, 255)] = 5;


  Mat resulting_matrix = Mat::zeros(test_image.size(), CV_8UC1);
  std::cout << resulting_matrix.size() << std::endl;
  auto test_it = test_image.begin<Vec3b>();
  auto test_end = test_image.end<Vec3b>();
  auto result_it = resulting_matrix.begin<uchar>();
  for (; test_it != test_end; ++test_it, ++result_it) {
    int predicted_value = classes[(*test_it)];
    *result_it = predicted_value;
  }

  imwrite(argv[2], resulting_matrix);
}
