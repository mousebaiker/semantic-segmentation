if g++ -std=c++11 conversion_tool.cpp -o convert `pkg-config --cflags --libs opencv` ; then
  for i in ~/semantic-segmentation/Vaihingen/contest/gts_for_participants/*.tif
  do
    j=${i/gts_for_participants/gts_converted}
    echo $j
    ./convert $i $j 2>&1 | cat
  done
fi
