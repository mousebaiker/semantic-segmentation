import cv2
import numpy as np
import sys

from data_utils import convert_image_to_classes
from helper_tools import get_shape, eprint, add_spaces
from paths import VAIH
import superpixel

names_mapping = [
  'imp_surf',
  'building',
  'low_veg',
  'tree',
  'car',
  'background'
]


def calcucate_statistics(matrix):
  """
  Calculate additional statistics, i.e. recall, precision and f1 score for
  each class and overall accuracy across all classes.

  Input: confusion matrix of shape (N, N). ith row, jth column contains
  the number of times jth class has been labeled as ith.

  Output:  Tuple with calculated statistics:
    (Matrix normalized along rows, recall, precision, f1 score, accuracy)
  """
  confusion_matrix_normalized = matrix/matrix.sum(axis=1, keepdims=True)
  recall = matrix.diagonal()/(np.sum(matrix, axis=0))
  precision = matrix.diagonal()/(np.sum(matrix, axis=1))
  f1_score = 2 * precision * recall/(precision + recall)
  overall_accuracy = matrix.diagonal().sum()/matrix.sum()

  return (confusion_matrix_normalized, recall, precision, f1_score,
          overall_accuracy)


def print_statistics(stats):
  (confusion_matrix_normalized, recall, precision,
  f1_score, overall_accuracy) = stats
  width = 10
  header = '|'.join([add_spaces(i, width) for i in names_mapping])
  bars = '+'.join(['-' * width for i in range(len(names_mapping) + 1)])
  bars = '+' + bars + '+'
  header = '|' + ' ' * width + '|' + header + '|'

  print(bars)
  print(header)
  print(bars)

  for i, row in enumerate(confusion_matrix_normalized):
    row_strings = []
    for value in row:
      row_strings.append(add_spaces("{:.3}".format(value), width))
    print('|' + add_spaces(names_mapping[i], width), '|'.join(row_strings), '',
          sep = '|')

  precision_values = '|'.join([add_spaces("{:.3}".format(i), width)
                               for i in precision])
  recall_values = '|'.join([add_spaces("{:.3}".format(i), width)
                               for i in recall])
  f1_values = '|'.join([add_spaces("{:.3}".format(i), width)
                               for i in f1_score])
  print(bars)
  print('', add_spaces('Precision', width), precision_values, '', sep = '|')
  print(bars)
  print('', add_spaces('Recall', width), recall_values, '', sep = '|')
  print(bars)
  print('', add_spaces('F1 score', width), f1_values, '', sep = '|')
  print(bars)
  print('Overall accuracy:', overall_accuracy)


def evaluate_classification(test_image, ground_truth, convert=True):
  number_of_classes = len(names_mapping)
  confusion_matrix = np.zeros((number_of_classes + 2, number_of_classes + 2),
      dtype=np.uint64)

  test_classes = test_image
  true_classes = ground_truth
  if (convert):
    test_classes = convert_image_to_classes(test_image).reshape((-1))
    true_classes = convert_image_to_classes(ground_truth).reshape((-1))
  np.add.at(confusion_matrix, [test_classes, true_classes], 1)

  # Restore proposed order of classes and delete redundant entries.
  entries_order = np.array([7, 4, 6, 2, 3, 1])
  confusion_matrix = confusion_matrix[:, entries_order][entries_order, :]
  return confusion_matrix


def Run():
  test_image = cv2.imread(VAIH + 'gts_for_participants/' +
                                    'top_mosaic_09cm_area1.tif')
  ground_truth = cv2.imread(VAIH + 'gts_for_participants/' +
                                      'top_mosaic_09cm_area1_spoiled.tif')
  if test_image is None or ground_truth is None:
    eprint("Error opening the file")
    return 1

  confusion_matrix = evaluate_classification(test_image, ground_truth)
  print(confusion_matrix)
  stats = calcucate_statistics(confusion_matrix)
  print_statistics(stats)


def test():
  confusion_matrix = np.array([[1, 2, 1], [2, 1, 2], [1, 1 ,2]])
  confusion_matrix2 = np.array([[1, 2, 3], [4, 5, 6], [7, 8 ,9]])
  result = np.zeros((3, 3))
  it = np.nditer([confusion_matrix, confusion_matrix2, result], [],
                 [['readonly'], ['readonly'], ['writeonly']])
  for (x, y, z) in it:
    z[...] = x + y
    print(x, y, z)
  print(it.operands[2])
  print_statistics(calcucate_statistics(confusion_matrix))


def parse_input_matrix():
  buffer = []
  for line in sys.stdin:
    line = line.strip()
    if line[0] == '[':
      line = line[1:]
    if line[-1] == ']':
      line = line[:-1]
    if line[-1] == ';':
      line = line[:-1]
    buffer.append([int(x.strip()) for x in line.split(',')])
  return buffer


def superpixeling_evaluation():
  test_image = cv2.imread(VAIH + 'top/' +
      'top_mosaic_09cm_area1.tif')
  ground_truth = cv2.imread(VAIH + 'gts_for_participants/' +
      'top_mosaic_09cm_area1.tif')
  ground_truth = convert_image_to_classes(ground_truth)
  images = [test_image]
  for image in images:
    for algorithm in ["Turbopixel", "SLIC"]:
      pixels = superpixel.SuperPixels(image)
      pixels.superpixelate(algorithm, show_clustering=False)
      classes = pixels.get_classes(ground_truth)
      labels = pixels.seeds.getLabels()
      result = classes[labels]
      print(algorithm, 'accuracy:', np.mean(result == ground_truth))

      ## Fun to look at.
      # values = pixels.get_superpixel_values()
      # cv2.imshow(algorithm, values[labels].astype('int8'))
      # cv2.waitKey(0)

def print_help():
  print("Run w/o parameters to start evaluation.")
  print("-f -- formating mode which accepts raw confusion matrix as an input"
        "and output statistics calculated on that matrix.")
  print("-s -- superpixel comparision mode.")
  print("-t -- test mode.")

if __name__ == "__main__" :
  if len(sys.argv) == 1:
    print("Started calculating, this can take long time.")
    Run()
  elif sys.argv[1] == '-f':
    matrix = parse_input_matrix()
    stats = calcucate_statistics(np.array(matrix))
    print_statistics(stats)
  elif sys.argv[1] == '-t':
    test()
  elif sys.argv[1] == '-s':
    superpixeling_evaluation()
  elif sys.argv[1] == '-h' or sys.argv[1] == '--help':
    print_help()
  else:
    eprint("Incorrect arguments!")
