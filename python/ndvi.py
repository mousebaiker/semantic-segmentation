import numpy as np

def ndvi(samples):
    """
    Computes NDVI for the sample.

    Input:
      samples - array of shape (num_of_pixels, 3), containing pixels in the
      format (Green, Red, InfraRed).
    Output:
      array of shape (num_of_pixels,), containing the NDVI value corresponding
      to the pixel.
    """
    sample = np.copy(samples)
    sample[:, 2] = np.maximum(sample[:, 2], 1)
    return sample[:, 2] - sample[:, 1]
    # return ((sample[:, 2] - sample[:, 1])/
    #   (sample[:, 2] + sample[:, 1]) * 255).astype('int8')


if __name__ == '__main__':
    import data_utils
    import numpy as np
    from sklearn.ensemble import RandomForestClassifier

    # Prepare data.
    x,y = data_utils.load_vaihingen()
    for i in range(len(x)):
        x[i] = x[i].reshape((-1, 3)).astype(np.dtype('int16'))
        y[i] = y[i].reshape((-1))
    x_train = x[:10]
    x_test = x[10:]
    y_train = y[:10]
    y_test = y[10:]
    x_train = np.concatenate(x_train)
    y_train = np.concatenate(y_train)
    x_test = np.concatenate(x_test)
    y_test = np.concatenate(y_test)
    x_train_ndvi = ndvi(x_train)
    y_train = ((y_train == data_utils.CLASSES['low_veg']) |
              (y_train == data_utils.CLASSES['tree']))
    x_test_ndvi = ndvi(x_test)
    y_test = ((y_test == data_utils.CLASSES['low_veg']) |
              (y_test == data_utils.CLASSES['tree']))
    print('Conversion complete')

    # Threshold classification.
    print(np.mean(((x_train_ndvi >= 0.3) & (x_train_ndvi <= 0.9)) == y_train))

    # Random forest classification.
    # ids = np.random.choice(np.arange(x_train.shape[0]), size=10**5)
    # x_sample = x_train[ids]
    # y_sample = y_train[ids]
    # forest = RandomForestClassifier()
    # forest.fit(x_sample, y_sample)
    # print('Fitting complete')
    # ids = np.random.choice(np.arange(x_test.shape[0]), size=10**5)
    # x_test_sample = x_test[ids]
    # y_test_sample = y_test[ids]
    # y_predict = forest.predict(x_test_sample)
    # print(np.mean(y_predict == y_test_sample))
