import cv2
import numpy as np
import sys
from helper_tools import get_shape, eprint
from paths import POSTDAM, VAIH

class SuperPixels(object):
  def __init__(self, image, seeds=None):
    self.image = image
    self.seeds = seeds

  def superpixelate(self, algorithm_type, show_clustering=False):
    converted_image = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
    seeds = None
    if algorithm_type == "Turbopixel":
      num_superpixels = 400
      num_iterations = 4
      prior = 0
      num_levels = 4
      num_histogram_bins = 10

      height, width, channels = get_shape(converted_image)

      seeds = cv2.ximgproc.createSuperpixelSEEDS(width, height, channels,
          num_superpixels, num_levels, prior, num_histogram_bins)
      seeds.iterate(converted_image, num_iterations)
    elif algorithm_type == "SLIC":
      num_iteratations = 10

      seeds = cv2.ximgproc.createSuperpixelSLIC(converted_image)
      seeds.iterate(num_iteratations)
    else:
      eprint("Unrecognized algorithm type")
      return

    if (show_clustering):
      labels = seeds.getLabels()
      mask = seeds.getLabelContourMask(True)
      mask_inv = cv2.bitwise_not(mask)
      background = cv2.bitwise_and(self.image,  self.image, mask=mask_inv)
      foreground = cv2.bitwise_and(converted_image, converted_image, mask=mask)
      result = background
      if algorithm_type == "Turbopixel":
        result = cv2.add(background, foreground)
      cv2.imshow(algorithm_type, result)
      cv2.waitKey(0)

    self.seeds = seeds

  def get_superpixel_values(self):
    """
    Get the mean pixel value for each superpixel.
    """
    result = []
    for i in range(self.image.shape[2]):
      channel = self.image[:,:,i].ravel()
      pixel_sum = np.bincount(self.seeds.getLabels().ravel(), weights=channel)
      pixel_count = np.maximum(np.bincount(self.seeds.getLabels().ravel()), 1)
      pixel_mean = (pixel_sum/pixel_count).astype(np.dtype('int'))
      result.append(pixel_mean)

    return np.vstack(result).T

  def get_classes(self, y):
    """
    Get class for each superpixel based on classes of pixels.
    """
    number_of_classes = np.max(y) + 1
    result = np.zeros(self.seeds.getNumberOfSuperpixels())
    for i in range(number_of_classes):
      a = np.bincount(self.seeds.getLabels().ravel(), weights=(y==i).ravel())
      result = np.vstack((result, a))
    return np.argmax(result, axis=0) - 1

  def get_stats(self, pixel_stat, func=np.mean):
    # number_of_labels = self.seeds.getNumberOfSuperpixels()
    # labels = self.seeds.getLabels()
    # result = []
    # for i in range(number_of_labels):
    #   if np.any(labels == i):
    #     result.append(func(pixel_stat[labels == i]))
    #   else:
    #     result.append(0)
    # return np.array(result)

    labels = self.seeds.getLabels()
    number_of_supers = self.seeds.getNumberOfSuperpixels()
    sorted_indexes = np.argsort(labels.ravel())
    sorted_labels = labels.ravel()[sorted_indexes]
    changes = (sorted_labels[1:] != sorted_labels[:-1]).nonzero()[0] + 1

    result = np.zeros(number_of_supers)
    splitted = np.split(pixel_stat.ravel()[sorted_indexes], changes)
    changes -= 1
    for i, elem in enumerate(splitted):
      if i == len(splitted) - 1:
        result[sorted_labels[-1]] = func(elem)
      else:
        result[sorted_labels[changes[i]]] = func(elem)
    return result

  def get_neighbours(self):
    """
    Return adjacency lists for superpixels.
    """
    number_of_supers = self.seeds.getNumberOfSuperpixels()

    labels = self.seeds.getLabels()
    append_column = (np.zeros(labels.shape[0]) - 1).reshape((-1, 1))
    right_shifted = np.hstack((append_column, labels[:,0:-1]))
    left_shifted = np.hstack((labels[:, 1:], append_column))

    append_row = (np.zeros(labels.shape[1]) - 1)
    down_shifted = np.vstack((append_row, labels[0:-1,]))
    up_shifted = np.vstack((labels[1:,], append_row))

    shifts = [right_shifted, left_shifted, up_shifted, down_shifted]
    sorted_indexes = np.argsort(labels.ravel())
    sorted_labels = labels.ravel()[sorted_indexes]
    changes = (sorted_labels[1:] != sorted_labels[:-1]).nonzero()[0] + 1
    changed_shifts = [np.split(shift.ravel()[sorted_indexes], changes)
                      for shift in shifts]
    result = [[] for _ in range(number_of_supers)]
    changes -= 1
    for i in range(len(changed_shifts[0])):
      resulting_array = np.unique(np.concatenate([changed_shifts[j][i]
                        for j in range(len(changed_shifts))]))
      mask = (resulting_array != i) & (resulting_array != -1)
      if i == len(changed_shifts[0]) - 1:
        result[-1].append(resulting_array[mask])
      else:
        result[sorted_labels[changes[i]]].append(resulting_array[mask])
    return result


class Seeds(object):
  def __init__(self, labels):
    self.labels = labels
    self.number_of_sp = np.max(labels)

  def getLabels(self):
    return self.labels

  def getNumberOfSuperpixels(self):
    return self.number_of_sp


def Run():
  import time
  input_image = cv2.imread(VAIH + 'top/' +
                                  'top_mosaic_09cm_area1.tif')
  if input_image is None:
    eprint("Error opening the file")
    return 1

  resized_image = cv2.resize(input_image, (0, 0), fx=1/1, fy=1/1)
  algorithm = "SLIC"
  if (len(sys.argv) > 1):
    algorithm = sys.argv[1]
  superpixel = SuperPixels(resized_image)
  a = time.time()
  superpixel.superpixelate(algorithm_type=algorithm, show_clustering=False)
  b = time.time()
  pixels = superpixel.get_superpixel_values()
  c = time.time()
  stats = superpixel.get_stats()
  print(pixels.shape, pixels, superpixel.seeds.getNumberOfSuperpixels(),
      np.min(superpixel.seeds.getLabels()))
  print(b - a, c - b)
if __name__ == "__main__" :
  Run()
