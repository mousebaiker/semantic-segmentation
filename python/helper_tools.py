import cv2
import sys


def get_shape(img):
    if len(img.shape) == 2:
        return (img.shape[0], img.shape[1], 1)
    return img.shape


def eprint(*args, **kwargs):
  print(*args, file = sys.stderr, **kwargs)


def add_spaces(s, width):
  front_spaces = ' ' * ((width - len(s))//2)
  end_spaces = ' ' * (width - len(s) - (width - len(s))//2)
  return front_spaces + s + end_spaces
