import numpy as np
import os
import cv2
import superpixel
import ndvi

CLASSES = {
    'imp_surf' : 7,
    'building' : 1,
    'low_veg' : 3,
    'tree' : 2,
    'car' : 6,
    'background' : 4,
}

contest_path = os.path.abspath('../contest/')
input_folder = os.path.join(contest_path, 'top/')
ground_truth_folder = os.path.join(contest_path, 'gts_for_participants')
dsm_folder = os.path.join(contest_path, 'ndsm/')
test_numbers = [7, 15, 1]

def convert_image_to_classes(image):
    """
    Computes classes of Vaihingen contest based on pixel values.

    Input:
      image - array of shape N x M x 3, representing image from Vaihingen
      contest with pixel values corresponding to specific classes.
    Output:
      Array of shape (N, M) containing class values
    """
    classes = np.copy(image)
    classes //= 255
    return classes[:, :, 0]*4 + classes[:, :, 1]*2 + classes[:, :, 2]

def convert_classes_to_image(classes):
  """
  Reverse of function above.
  """
  image = np.zeros(classes.shape + (3,))
  image[:, :, 0] = classes//4 * 255
  image[:, :, 1] = (classes//2 & 1) * 255
  image[:, :, 2] = (classes & 1) * 255
  return image

def load_vaihingen(verbose=False, load_dsm=False):
    train_images = []
    gt_images = []
    dsm_images = []
    for file_name in os.listdir(path=ground_truth_folder):
      if not ('spoiled' in file_name  or 'cropped' in file_name):
        train_image = cv2.imread(os.path.join(input_folder, file_name))
        gt_image = cv2.imread(os.path.join(ground_truth_folder,
                                           file_name))
        dsm_image = None
        if load_dsm:
          file_name = file_name.replace('top_mosaic_09cm', 'ndsm_09cm_matching').replace('tif', 'bmp')
          dsm_image = cv2.imread(os.path.join(dsm_folder, file_name), cv2.CV_8UC1)
          print(os.path.join(dsm_folder, file_name))
        if train_image is not None and gt_image is not None:
          train_images.append(train_image)
          gt_images.append(convert_image_to_classes(gt_image))
          if load_dsm and dsm_image is not None:
            dsm_images.append(dsm_image)
          if verbose:
            print(file_name, train_image.shape, gt_image.shape)
    return_value = (train_images, gt_images)
    if load_dsm:
      return_value = (train_images, gt_images, dsm_images)
    return return_value
    # return np.concatenate(train_images), np.concatenate(gt_images)

def load_suffix_files(suffix):
  output_folder = os.path.join(contest_path, 'calculated/')
  train_sp = []
  test_sp = []
  train_indexes = []
  test_indexes = []
  for file_name in os.listdir(path=ground_truth_folder):
    if not ('spoiled' in file_name  or 'cropped' in file_name):
      image_number = int(file_name.replace('top_mosaic_09cm_area', '').replace('.tif',''))
      prefix = 'train_'
      if image_number in test_numbers:
        prefix = 'test_'

      image_name = prefix + str(image_number)
      result = np.loadtxt(os.path.join(output_folder, image_name + suffix), delimiter=',')

      if prefix == 'train_':
        train_sp.append(result)
        train_indexes.append(image_number)
      else:
        test_sp.append(result)
        test_indexes.append(image_number)
  return (train_sp, test_sp, train_indexes, test_indexes)

def calculate_information_for_vaihingen():
  output_folder = os.path.join(contest_path, 'calculated/')
  done = []
  train_sp = []
  test_sp = []

  for file_name in os.listdir(path=ground_truth_folder):
    if not ('spoiled' in file_name  or 'cropped' in file_name):
      image_number = int(file_name.replace('top_mosaic_09cm_area', '').replace('.tif',''))
      if image_number in done:
        continue
      print(image_number)
      prefix = 'train_'
      if image_number in test_numbers:
        prefix = 'test_'
      train_image = cv2.imread(os.path.join(input_folder, file_name))
      converted_image = cv2.cvtColor(train_image, cv2.COLOR_BGR2HSV)
      gt_image = cv2.imread(os.path.join(ground_truth_folder,
                                         file_name))
      gt_converted = convert_image_to_classes(gt_image)
      dsm_image_name = file_name.replace('top_mosaic_09cm', 'ndsm_09cm_matching').replace('tif', 'bmp')
      ndsm_image = cv2.imread(os.path.join(dsm_folder, dsm_image_name), cv2.CV_8UC1)
      image_name = prefix + str(image_number)
      print('gt_converted', gt_converted.shape)
      np.savetxt(os.path.join(output_folder, image_name + '_gt'),
          gt_converted, delimiter=',', fmt='%d')

      x_ndvi = ndvi.ndvi(train_image.reshape((-1, 3))).reshape(train_image.shape[:2])
      cv2.imwrite(os.path.join(output_folder, image_name + '_ndvi.tiff'), x_ndvi)

      print('pixelating')
      sp = superpixel.SuperPixels(train_image)
      sp.superpixelate("SLIC")
      np.savetxt(os.path.join(output_folder, image_name + '_splabels'),
          sp.seeds.getLabels(), delimiter=',', fmt='%d')
      print('stats')
      mean_values = sp.get_superpixel_values()
      std_r = sp.get_stats(train_image[:,:,2], func=np.std)
      std_g = sp.get_stats(train_image[:,:,1], func=np.std)
      std_b = sp.get_stats(train_image[:,:,0], func=np.std)
      sp_ndvi = ndvi.ndvi(mean_values)
      brightness = sp.get_stats(converted_image[:,:,2], func=np.mean)
      std_brightness = sp.get_stats(converted_image[:,:,2], func=np.std)
      ndsm = sp.get_stats(ndsm_image, func=np.mean)
      std_ndsm = sp.get_stats(ndsm_image, func=np.std)
      gt = sp.get_classes(gt_converted)

      print(mean_values.shape, std_r.shape, std_g.shape, std_b.shape,
          sp_ndvi.shape, brightness.shape, std_brightness.shape,
          ndsm.shape, std_ndsm.shape, gt.shape)

      result = np.vstack((mean_values.T, std_r, std_g, std_b, sp_ndvi, brightness,
        std_brightness, ndsm, std_ndsm, gt)).T
      np.savetxt(os.path.join(output_folder, image_name + '_sp'), result,
        delimiter=',')

  train_sp, test_sp, _, _ = load_suffix_files('_sp')

  train_values = np.vstack(train_sp)
  test_values = np.vstack(test_sp)
  print(train_values.shape, test_values.shape)
  np.savetxt(os.path.join(output_folder,'full_sp_train'), train_values,
      delimiter=',')
  np.savetxt(os.path.join(output_folder,'full_sp_test'), test_values,
      delimiter=',')

def load_superpixels():
  contest_path = os.path.abspath('../contest/')
  input_folder = os.path.join(contest_path, 'calculated/')
  train_file = os.path.join(input_folder, 'full_sp_train')
  test_file = os.path.join(input_folder, 'full_sp_test')


  train = np.loadtxt(train_file, delimiter=',')
  test = np.loadtxt(test_file, delimiter=',')
  train_x = train[:, :-1]
  train_y = train[:, -1].astype('int')
  del train
  test_x = test[:, :-1]
  test_y = test[:, -1].astype('int')
  del test

  return (train_x, train_y, test_x, test_y)

def calculate_neighbours():
  input_folder = os.path.join(contest_path, 'calculated/')

  (train_labels, test_labels,
      train_indexes, test_indexes) = load_suffix_files('_splabels')

  for i, labels in enumerate(train_labels):
    labels = labels.astype('int')
    sp = superpixel.SuperPixels(None, superpixel.Seeds(labels))
    neighs = sp.get_neighbours()
    print(i, neighs)

def generate_images(superpixel_y):
  test_images = [7, 15, 1]
  input_folder = os.path.join(contest_path, 'calculated/')

  offset = 0
  result = []
  for image_number in test_images:
    path = os.path.join(input_folder, 'test_' + str(image_number) + '_splabels')
    labels = np.loadtxt(path, delimiter=',').astype('int')
    number_of_sp = np.max(labels) + 1
    image = (superpixel_y[offset:offset + number_of_sp])[labels]
    result.append(convert_classes_to_image(image))
    offset += number_of_sp
  print(offset, superpixel_y.shape)
  return result

if __name__ == '__main__':
    # x,y = load_vaihingen()
    # print(x.shape, y.shape)
    calculate_information_for_vaihingen()
