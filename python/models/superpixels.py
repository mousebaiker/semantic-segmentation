import data_utils
import superpixel
import evaluation_tool as eval_tool
import numpy as np
import ndvi
import cv2
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':
  # Prepare data
  x, y, dsm = data_utils.load_vaihingen(verbose=True, load_dsm=True)

  x_converted = []
  y_converted = []
  sp_sizes = []
  sps = []
  for i in range(len(x)):
    superpixels = superpixel.SuperPixels(x[i])
    superpixels.superpixelate("SLIC")
    sps.append(superpixels)
    print('Done')
    mean_values = superpixels.get_superpixel_values()
    x_converted.append(mean_values)
    # max_values_r = superpixels.get_stats(x[i][:,:,0], func=np.max)
    # max_values_g = superpixels.get_stats(x[i][:,:,1], func=np.max)
    # max_values_b = superpixels.get_stats(x[i][:,:,2], func=np.max)
    # x_ndvi = ndvi.ndvi(x[i].reshape((-1,3))).reshape(x[i].shape)
    gt = superpixels.get_classes(y[i])
    y_converted.append(gt)
    sp_sizes.append(mean_values.shape[0])
    print(mean_values.shape, gt.shape)


  number_of_training = 10
  number_of_validation = 3
  x_tr = x_converted[:number_of_training]
  x_val = x_converted[number_of_training : number_of_training +
      number_of_validation]
  x_te = x_converted[number_of_training + number_of_validation:]
  y_tr = y_converted[:number_of_training]
  y_val = y_converted[number_of_training : number_of_training +
      number_of_validation]
  y_te = y_converted[number_of_training + number_of_validation:]
  x_train = np.concatenate(x_tr)
  del x_tr
  x_validation = np.concatenate(x_val)
  del x_val
  y_validation = np.concatenate(y_val)
  del y_val
  y_train = np.concatenate(y_tr)
  del y_tr
  x_test = np.concatenate(x_te)
  del x_te
  y_test = np.concatenate(y_te)
  del y_te

  print(x_train.shape, y_train.shape, x_validation.shape, y_validation.shape, x_test.shape, y_test.shape )


  print("Start crossvalidation")
  crits = ['gini', 'entropy']
  features = [1, 3]
  min_samples = [1, 5]
  validation = {}
  best_accuracy = 0
  best_stats = None
  best_classifier = None
  for crit in crits:
    for feature in features:
      for min_sample in min_samples:
        forest = RandomForestClassifier(criterion=crit, max_features=feature,
            min_samples_leaf=min_sample)
        forest.fit(x_train, y_train)
        y_train_predict = forest.predict(x_train)
        y_predict = forest.predict(x_validation)
        matrix = eval_tool.evaluate_classification(y_predict,
            y_validation, False)
        stats = eval_tool.calcucate_statistics(matrix)
        validation[(crit, feature, min_sample)] = stats
        if stats[-1] > best_accuracy:
          best_accuracy = stats[-1]
          best_stats = stats
          best_classifier = forest

  for crit, feature, min_sample in validation:
    print("Accuracy of:", validation[(crit, feature, min_sample)][-1],
        "with criterion", crit, "max features", feature, "min samples per leaf",
        min_sample)
  eval_tool.print_statistics(best_stats)

  ## Test
  test_offset = number_of_training + number_of_validation
  test_sizes = sp_sizes[test_offset:]
  splits = [0,]
  cum_sum = 0
  for i in test_sizes:
    cum_sum += i
    splits.append(cum_sum)
  y_test_images = np.split(y_test, splits[1:])

  for i in range(len(splits) - 1):
    test = x_test[splits[i]:splits[i + 1]]
    prediction = forest.predict(test).astype('int8')
    supers = sps[test_offset + i]
    ans_matrix = prediction[supers.seeds.getLabels()]
    matrix = eval_tool.evaluate_classification(ans_matrix,
        y[test_offset + i], False)
    stats = eval_tool.calcucate_statistics(matrix)
    eval_tool.print_statistics(stats)
    image = data_utils.convert_classes_to_image(ans_matrix)
    cv2.imwrite("models/superpixels/test" + str(i) + ".jpg", image)
