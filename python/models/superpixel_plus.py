import cv2
import data_utils
import evaluation_tool as eval_tool
import ndvi
import numpy as np
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':
  train_x, train_y, test_x, test_y = data_utils.load_superpixels()

  number_of_train_pixels = 5*10**5

#  train_x[:, 0:3] -= np.mean(train_x[:, 0:3], axis=0)

  print(np.mean(train_x[:, 0]))

  t_x = train_x[:number_of_train_pixels]
  t_y = train_y[:number_of_train_pixels]
  v_x = train_x[number_of_train_pixels:]
  v_y = train_y[number_of_train_pixels:]

  del train_x
  del train_y

  print("Start crossvalidation")
  number_of_trees = [50]
  features = [5]
  min_samples = [5, 9]
  validation = {}
  best_accuracy = 0
  best_stats = None
  best_classifier = None
  for n_trees in number_of_trees:
    for feature in features:
      for min_sample in min_samples:
        forest = RandomForestClassifier(n_estimators = n_trees,
            max_features=feature,
            min_samples_leaf=min_sample)
        forest.fit(t_x, t_y)
        y_train_predict = forest.predict(t_x)
        y_predict = forest.predict(v_x)
        matrix = eval_tool.evaluate_classification(y_predict,
            v_y, False)
        stats = eval_tool.calcucate_statistics(matrix)
        validation[(n_trees, feature, min_sample)] = stats
        eval_tool.print_statistics(stats)
        if stats[-1] > best_accuracy:
          best_accuracy = stats[-1]
          best_stats = stats
          best_classifier = forest
        print('One done')

  for n_trees, feature, min_sample in validation:
    print("Accuracy of:", validation[(n_trees, feature, min_sample)][-1],
        "with trees", n_trees, "max features", feature, "min samples per leaf",
        min_sample)
  eval_tool.print_statistics(best_stats)

 # Test
  print('Test results:')
  test_predict = best_classifier.predict(test_x)
  matrix = eval_tool.evaluate_classification(test_predict, test_y, False)
  stats = eval_tool.calcucate_statistics(matrix)
  eval_tool.print_statistics(stats)
  images = data_utils.generate_images(test_predict)
  for i, image in enumerate(images):
    cv2.imwrite("models/superpixels_plus_ndsm/test" + str(i) + ".jpg", image)
