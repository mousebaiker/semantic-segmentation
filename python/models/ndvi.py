import cv2
import data_utils
import evaluation_tool as eval_tool
import ndvi
import numpy as np
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':
  # Prepare data
  x, y = data_utils.load_vaihingen(verbose=True)
  sizes = []
  for i in range(len(x)):
    sizes.append(x[i].shape)
    x[i] = x[i].reshape((-1, 3)).astype(np.dtype('int16'))
    x_ndvi = ndvi.ndvi(x[i]).reshape((-1, 1))
    x[i] = np.hstack((x[i], x_ndvi))
    y[i] = y[i].reshape((-1))


  number_of_training = 10
  number_of_validation = 3
  x_train = x[:number_of_training]
  x_validation = x[number_of_training : number_of_training +
      number_of_validation]
  x_test = x[number_of_training + number_of_validation:]
  y_train = y[:number_of_training]
  y_validation = y[number_of_training : number_of_training +
      number_of_validation]
  y_test = y[number_of_training + number_of_validation:]
  x_train = np.concatenate(x_train)
  x_validation = np.concatenate(x_validation)
  y_validation = np.concatenate(y_validation)
  y_train = np.concatenate(y_train)
  x_test = np.concatenate(x_test)
  y_test = np.concatenate(y_test)

  ids = np.random.choice(np.arange(x_train.shape[0]), size=5 * 10**5)
  x_sample = x_train[ids]
  y_sample = y_train[ids]

  # Crossvalidate training
  crits = ['gini', 'entropy']
  features = [1, 3]
  min_samples = [1, 5]
  val_ids = np.random.choice(np.arange(x_validation.shape[0]), size=10**5)
  validation = {}
  best_accuracy = 0
  best_stats = None
  best_classifier = None
  for crit in crits:
    for feature in features:
      for min_sample in min_samples:
        forest = RandomForestClassifier(criterion=crit, max_features=feature,
            min_samples_leaf=min_sample)
        forest.fit(x_sample, y_sample)
        y_train_predict = forest.predict(x_sample)
        y_predict = forest.predict(x_validation[val_ids])
        matrix = eval_tool.evaluate_classification(y_predict,
            y_validation[val_ids], False)
        stats = eval_tool.calcucate_statistics(matrix)
        validation[(crit, feature, min_sample)] = stats
        if stats[-1] > best_accuracy:
          best_accuracy = stats[-1]
          best_stats = stats
          best_classifier = forest

  for crit, feature, min_sample in validation:
    print("Accuracy of:", validation[(crit, feature, min_sample)][-1],
        "with criterion", crit, "max features", feature, "min samples per leaf",
        min_sample)
  eval_tool.print_statistics(best_stats)

  # Test
  test_sizes = sizes[number_of_training + number_of_validation:]
  splits = [0,]
  cum_sum = 0
  for i in test_sizes:
    cum_sum += np.prod(i[:2])
    splits.append(cum_sum)
  y_test_images = np.split(y_test, splits[1:])
  for i in range(len(splits) - 1):
    split_images = np.array_split(x_test[splits[i]:splits[i + 1]], 100)
    image_results = []
    for j, split_image in enumerate(split_images):
      image_results.append(forest.predict(split_image).astype('int8'))
    y_predict = np.concatenate(image_results)
    matrix = eval_tool.evaluate_classification(y_predict,
        y_test_images[i], False)
    stats = eval_tool.calcucate_statistics(matrix)
    eval_tool.print_statistics(stats)
    image = data_utils.convert_classes_to_image(
        y_predict.reshape(test_sizes[i][:2]))
    cv2.imwrite("models/ndvi/test" + str(i) + ".jpg", image)
