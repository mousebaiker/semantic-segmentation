# Installation requirements

This document will walk you through installation of necessary tools on Linux Ubuntu.

### Virtual environment (Optional)
If you are not willing to pollute the system python, you can use virtual
environment.
To set up a virtual environment, run the following:

```bash
# Get the virtualenv for Python3
sudo pip3 install virtualenv
# Create a new environment in .env dir
virtualenv .env
# Activate it
source .env/bin/activate

# Deactivate to return to normal bash session
deactivate
```

### OpenCV 3.1.0
#### Credit: http://stackoverflow.com/a/21212023

#### Prerequisites
```bash
# Required libraries
sudo apt install build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

# If you don't use virtualenv
sudo apt install python3.5-dev libpython3-dev python3-numpy
# If you use it
pip3 install numpy scipy sklearn

# Codecs
sudo apt install libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
```

#### Installation
Basically, we are going to build it from source using cmake.

```bash
# Choose the directory where to download the source and get it from github.
git clone https://github.com/opencv/opencv
git clone https://github.com/opencv/opencv_contrib.git

# Go to directory with source code and checkout the specific version.
cd opencv
git checkout 3.1.0
cd ../opencv_contrib
git checkout 3.1.0
cd ../opencv

# Create a directory to put the OpenCV pre-build generated files.
mkdir build
cd build

# Prepare build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local  ..

# If you are using virtualenv, this flags could be added to the following command to make opencv point to the right python binary
cmake -D PYTHON_DEFAULT_EXECUTABLE=../../.env/bin/python ..
cmake -D PYTHON_INCLUDE_DIRS=../../.env/include/python3.5m ..
cmake -D PYTHON_EXECUTABLE=../../.env/bin/python3 ..
cmake -D PYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.5m.so.1 ..

# Enable documentation and disable tests
cmake -D BUILD_DOCS=ON -D BUILD_TESTS=OFF -D BUILD_PERF_TESTS=OFF -D BUILD_EXAMPLES=OFF -D INSTALL_C_EXAMPLES=OFF ..

# Include additional modules which are not provided with standart OpenCV library.
cmake -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules ..

# Build the library (-jN uses N cores to install the library)
make -j2

# A quick fix to make install script use the right folder
ln -s /usr/local/lib/python3.5/site-packages/cv2.cpython-35m-x86_64-linux-gnu.so ../../.env/lib/python3.5/site-packages/

# Install the binaries
sudo make install
```
